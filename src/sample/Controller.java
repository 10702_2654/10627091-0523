package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller  implements Initializable {
    public Button btworing;
    public ProgressBar pbworking;
    public ImageView IV;

    public void onworking(ActionEvent actionEvent) {

        Task task =new Task<Void>(){

            @Override
            protected Void call() throws Exception {
                final int max=100;
                for (int i = 1; i < max; i++) {
                    Thread.sleep(75);
                    updateProgress(i, max);
                }
                return null;
            }
            @Override
            protected void succeeded(){
                super.succeeded();
                pbworking.setVisible(false);
                IV.setVisible(false);
            }
        };
        pbworking.progressProperty().bind(task.progressProperty());
        pbworking.setVisible(true);
        new Thread(task).start();
        List<String> imagepath=new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            imagepath.add("/resources/".concat(String.valueOf(i)).concat(".png"));
        }
        Timeline timeline =new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setAutoReverse(false);
        for (int i = 0; i < 5; i++) {
            int j=i;
            timeline.getKeyFrames().add(
            new KeyFrame(Duration.seconds(i + 1), new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    IV.setImage(new Image(getClass().getResource(imagepath.get(j)).toString()));
                }
            })
            );
        }
        timeline.play();

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //pbworking.setProgress(ProgressBar.INDETERMINATE_PROGRESS);
        //pbworking.setVisible(false);


    }
}
